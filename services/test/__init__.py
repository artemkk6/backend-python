from common.base.base_handler import BaseHandler
from services.test.model import Test
from settings import Settings


class TestHandler(BaseHandler):
    __slots__ = ['model']

    settings = Settings()

    async def handle_ping(self, params: dict) -> str:
        result_bool_obj: str = self.model.pong if params.get('request') == 'ping' else '****'
        return result_bool_obj
        
    async def handle_get_generate_list(self, params: dict) -> list[int]:
        return list(
            set(
                self.model.generate_list(
                    params['index']
                )
            )
        )

    def __init__(self) -> None:
        self.model = Test()

        super(TestHandler, self).__init__(request_queue_uuid=self.settings.TEST_REQUEST_QUEUE)
        self.register_method_handlers(
            handlers={
                'ping': self.handle_ping,
                'handle_get_generate_list': self.handle_get_generate_list
            }
        )
