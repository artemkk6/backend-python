from settings import Settings
from typing import Generator
from random import randint

class Test:
    __slots__ = ['pong']

    settings = Settings()

    def __init__(self) -> None:
        self.pong = 'pong'

    def generate_list(self, count: int) -> Generator[int, None, None]:
        for _ in range(0, count):
            yield randint(0, 99)
